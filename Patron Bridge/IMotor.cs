﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Bridge
{
    // Implementacion
    public interface IMotor
    {
        //Metodos del motor 
        public void ConsumirCombustible();
        public void InyectarCombustible(double cantidad);
        


    }
}
