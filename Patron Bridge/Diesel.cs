﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Bridge
{
    //Clase que implementa la interfaz IMotor
    class Diesel :IMotor  
    {
        
        public void InyectarCombustible(double cantidad)
        {
            Console.WriteLine("Inyectando  " + cantidad + " ml. de Diesel");
        }
        public void ConsumirCombustible()
        {
            RealizarExplosion();
        }
        private void RealizarExplosion()
        {
            Console.WriteLine("Realizada la explosion del Diesel");
        }
    }
}
