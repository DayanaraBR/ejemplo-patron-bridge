﻿using System;

namespace Patron_Bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            //Mostraremos las porpiedades del auto en base a su estructura , Evolucionan idependientemente del motor 
            IMotor MotorDiesel = new Diesel();  
            Automovil berlina1 = new Berlina(MotorDiesel, 60);  
            berlina1.MostrarCaracteristicas();         //invocacion del Metodo Abtracto

          
            //ahora mostramos la funcionalidad  del auto de acuerdo a su trabajo de maquina 
            berlina1.Acelerar(2.6);  // invoca a los métodos  InyectarCombustible y ConsumirCombustible que implementa la clase Diesel de la interfaz IMotor
            berlina1.Frenar();      //invoca el metodo propio de la clase abstracta



            Console.ReadKey();
        }
    }
}
