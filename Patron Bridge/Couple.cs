﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Bridge
{
    class Couple : Automovil //hereda de  la clase abstracta Automovil
    {
        // Atributo propio
        private int NumeroPuertas;
        // constructor 
        public Couple(IMotor motor,int NumeroPuertas ) : base(motor) // La implementacion de los automoviles se desarrolla de forma independiente
        {
            this.NumeroPuertas = NumeroPuertas;
        }
        //  Implementacion de metodo abstracto
        public override void MostrarCaracteristicas()
        {
            Console.WriteLine("Automovil de tipo Couple tiene " +  NumeroPuertas +"puertas" );
        }
    }
}
