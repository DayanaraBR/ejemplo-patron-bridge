﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Bridge
{
    //Clase que implementa la interfaz IMotor
    class Gasolina : IMotor
    {
        
        public void InyectarCombustible(double cantidad)
        {
            Console.WriteLine("Inyectando  " + cantidad + " ml. de Gasolina");
        }
        public void ConsumirCombustible()
        {
            RealizarCombustion();
        }
        //Metodo propio
        private void RealizarCombustion()
        {
            Console.WriteLine("Realizada la combustion de la Gasolina");
        }

    }
}
