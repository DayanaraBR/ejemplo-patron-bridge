﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Patron_Bridge
{
    // Abstraccion 
   public abstract class Automovil
    {
        //Atributo 
        private IMotor motor;  //Donde el atributo puede ser  una clase de tipo  Diesel o Gasolina
        //Constructor 
        public Automovil(IMotor motor) => this.motor = motor;

        // Encapsulamos la funcionalidad de la interfaz IMotor
        public void Acelerar(double combustible)
        {
            Console.WriteLine("Acelerar");
            motor.InyectarCombustible(combustible);  
            motor.ConsumirCombustible();
            Console.WriteLine("El Automovil a Acelerado");
        }
        //Metodo propio 
        public void Frenar() // este metodo es independiente  de la interfaz 
        {
            Console.WriteLine("El vehículo está frenando.");
        }

        // Método abstracto
        public abstract void MostrarCaracteristicas(); // debe ser implementado en cada una de las clases hijas.



    }
}
