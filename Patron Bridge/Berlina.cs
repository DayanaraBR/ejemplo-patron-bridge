﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Bridge
{
    class Berlina :Automovil   //hereda de  la clase abstracta Automovil
    {
        //Atributo Propio
        private int capacidadCajuela;
        //Constructor
        public Berlina(IMotor motor, int capacidadCajuela):base(motor) //La implementacion de los automoviles  se desarrolla de forma independiente
        {
            this.capacidadCajuela = capacidadCajuela;
        }
        //  Implementacion de metodo abstracto
        public override void MostrarCaracteristicas()
        {
            Console.WriteLine("Automovil de tipo Berlina con un Capacidad de Cajuela de " + capacidadCajuela + " litros.");
        }

    }
}
